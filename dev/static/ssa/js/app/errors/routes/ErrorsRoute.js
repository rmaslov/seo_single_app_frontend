var ErrorRoute = Backbone.Router.extend({
    routes : {
        "*pageNotFound" : "p404"
    },

    p404:function(){
        var err404 = new Error404View();
        err404.render();
    }
});
