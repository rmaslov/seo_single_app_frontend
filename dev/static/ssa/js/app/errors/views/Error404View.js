var Error404View = Backbone.View.extend({
    templatePath: app.templatePath('errors/html/404.html'),

    render:function(){
        app.templateStore.template(this.templatePath, function(tmpl){
            $(app.mainBody).html(tmpl({}));
        });
        return this;
    }
});

