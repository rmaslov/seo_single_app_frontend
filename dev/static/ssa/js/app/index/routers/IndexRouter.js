var IndexRouter = Backbone.Router.extend({
    routes: {
        "test": "test",
        "": "index",
    },

    index: function () {
        app.views.pageLoad.render();
        var view = new IndexView();
        view.render();
        app.views.pageLoad.hide();

        $.ajax({
            type: 'GET',
            url: '/api/v1/blog/all?page=0',
            //dataType: 'jsonp',
            //callbackParameter: 'callback',
            //timeout: 10000,
            success: function(json){
                //alert(json.currentPage)
            },
            error: function(){
                //alert('error')
            }
        });
    },

    test: function () {
        app.views.pageLoad.render();
        var view = new IndexView();
        view.render('test');
        app.views.pageLoad.hide();
    }
});


app.startAfterInit.push(function () {
    var indexRouter = new IndexRouter();
});
