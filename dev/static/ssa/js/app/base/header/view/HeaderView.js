var HeaderView = Backbone.View.extend({
    templatePath: app.templatePath('base/header/html/header.html'),

    render: function () {
        app.templateStore.template(this.templatePath, function (template) {
            $('.js-navbar').html(template());
            $('.button-collapse').sideNav();
        });
        return this;
    }
});
