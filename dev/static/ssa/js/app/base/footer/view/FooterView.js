var FooterView = Backbone.View.extend({
    templatePath: app.templatePath('base/footer/html/footer.html'),

    render: function () {
        app.templateStore.template(this.templatePath, function (template) {
            $('.js-page-footer').html(template());
            $('.js-page-footer').removeClass('hide');

        });
        return this;
    }
});
