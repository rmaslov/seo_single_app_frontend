var PageLoadView = Backbone.View.extend({
    //template: _.template(),
    templatePath: app.templatePath('base/pageLoad/html/pageLoadView.html'),

    render: function () {
        var that = this;
        app.templateStore.template(this.templatePath, function (template) {
            $('.js-navbar-wrapper').append(template());
        });
        return this;
    },

    hide: function () {
        setTimeout(function () {
            $('.js-navbar-wrapper .js-navbar-wrapper-progress').animate({opacity:0}, 300, function(){
                $('.js-navbar-wrapper .js-navbar-wrapper-progress').remove();
            });
        }, 500);

    }
});

app.startAfterInit.push(function(){
    app.views.pageLoad = new PageLoadView();
});