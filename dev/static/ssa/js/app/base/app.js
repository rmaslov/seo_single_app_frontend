var TemplateStore = {
    templates: {},

    template: function (url, callback) {
        var temp = this.templates[url];
        if (temp) {
            callback(temp);
        } else {
            var that = this;
            $.get(url, function (template) {
                var tmpl = _.template(template);
                that.templates[url] = tmpl;
                callback(that.templates[url]);
            });
        }
    }
};

var app = {
    root: '/',
    jsBasePath: '/static/ssa/dist/app/',
    mainBody: '.js-main-body',
    templatePath: function (url) {
        return app.jsBasePath + url;
    },
    views: {},
    templateStore: TemplateStore,

    startAfterInit:[],
};



