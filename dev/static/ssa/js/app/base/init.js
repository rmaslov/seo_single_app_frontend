function backboneHistoryStart() {
    Backbone.history.start({pushState: true, root: app.root});

    $(document).on("click", "a[href]:not([data-bypass])", function (evt) {
        var href = {prop: $(this).prop("href"), attr: $(this).attr("href")};
        var root = location.protocol + "//" + location.host + app.root;

        if (href.prop.slice(0, root.length) === root) {
            evt.preventDefault();
            $("html, body").animate({ scrollTop: 0 }, "fast");
            Backbone.history.navigate(href.attr, true);
        }
    });

}

function initPage() {
    var footerView = new FooterView();
    footerView.render();

    var headerView = new HeaderView();
    headerView.render();


}

$(function () {
    //Добавляем обработку ошибок
    var errorRoute = new ErrorRoute();
    //Инициализируем контроллеры
    for(var key in app.startAfterInit){
        var func = app.startAfterInit[key];

        func();
    }


    backboneHistoryStart();
    initPage();
});

