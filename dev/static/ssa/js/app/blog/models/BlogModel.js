var BlogModel = Backbone.Model.extend({
    title:'',
    url:'',
    description:'',
    picId:''
});

var BlogCollection = Backbone.Collection.extend({
    model: BlogModel,

});