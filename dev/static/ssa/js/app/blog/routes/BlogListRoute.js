var BlogListRoute = Backbone.Router.extend({
    routes:{
        'blog':'blog',
        'blog/p:page':'blogList',
        'blog/:url':'blogPage',
    },

    blogCollection: new BlogCollection(),

    blog:function(){
        this.navigate('/blog/p0', true);
    },

    blogList:function(page){
        this.blogCollection = new BlogCollection();
        for(var i = 0; i < 18; i++){
            var blogModel = new BlogModel({
                title: "title",
                url: "url",
                description:'description',
                picId:'picId'
            });
            this.blogCollection.add(blogModel);
        }

        var blogListPageView = new BlogListPageView();
        blogListPageView.render(this.blogCollection);
    },

    blogPage:function(){
        var blogModel = new BlogModel({
            title: "title",
            url: "url",
            description:'description',
            picId:'picId'
        });

        var blogPage = new BlogPageView();
        blogPage.render(blogModel);
    }
});

app.startAfterInit.push(function(){
    var blogListRoute = new BlogListRoute();
});