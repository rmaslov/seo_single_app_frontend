var BlogCardView = Backbone.View.extend({
    templatePath: app.templatePath('blog/html/blogCard.html'),

    render: function(blogCard){
        var that = this;

        app.templateStore.template(this.templatePath, function(tmpl){
            that.$el.html(tmpl());
        });

        return this;
    }
});