var BlogListPageView = Backbone.View.extend({
    templatePath: app.templatePath('blog/html/blogListPage.html'),

    blogCollection: {},

    jqBlogList: '.js-blog-list',
    jqBlogPagination: '.js-blog-pagination',

    render: function(blogCollection){
        this.blogCollection = blogCollection;

        var that = this;
        app.templateStore.template(this.templatePath, function(tmpl){
            $(app.mainBody).html(tmpl());

            for(var key in that.blogCollection.models){
                var blogModel = that.blogCollection.models[key];

                var blogCard = new BlogCardView();
                $(that.jqBlogList).append(blogCard.render(blogModel).$el);
            }

            var pagination = new BlogPaginationView();
            $(that.jqBlogPagination).html(pagination.render().$el);
        });

        return this;
    }
});