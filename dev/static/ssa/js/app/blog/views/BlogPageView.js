var BlogPageView = Backbone.View.extend({
    templatePath: app.templatePath('blog/html/blogPage.html'),

    render: function(blogModel){
        app.templateStore.template(this.templatePath, function(tmpl){
            $(app.mainBody).html(tmpl());
        });
        return this;
    }
});