var BlogPaginationView = Backbone.View.extend({
    templatePath: app.templatePath('blog/html/blogPagination.html'),

    render:function(){
        var that = this;

        app.templateStore.template(this.templatePath, function(tmpl){
            that.$el.html(tmpl());
        });

        return this;
    }
});