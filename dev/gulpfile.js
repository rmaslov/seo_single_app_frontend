var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var webserver = require('gulp-webserver');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var gulpUtil = require('gulp-util');

var rootFolder = 'static/ssa/'
var backendFolder = './../../seo_single_app/src/main/webapp/'

/************ SASS ***************/
var sassWathTarget = rootFolder + 'sass/**/*.scss'

var sassTarget = rootFolder + 'sass/**/*.scss';
var sassDestDev = './' + rootFolder + 'css/'
var sassMakeProd = false
var sassDestProd = './../prod/static/sf/css'
var sassMakeBackend = true
var sassDestBackend = backendFolder + rootFolder + 'css'
/************ otherFiles ***************/
var fontWatchTarget = rootFolder + 'font/*'

var fontMakeBackend = true

var fontDestBackend = backendFolder + rootFolder + 'font'
/************ IMG ***************/
var imgWatchTarget = rootFolder + 'img/**/*.'

var imgTaskTarket = [rootFolder + 'img/**/*.jpg', rootFolder + 'img/**/*.png', rootFolder + 'img/**/*.jpeg', rootFolder + 'img/**/*.gif'];

var imgMakeProd = false
var imgMakeBackend = true

var imgDestProd = './../prod/' + rootFolder + 'img'
var imgDestBackend = backendFolder + rootFolder + 'img'

/************ JS ***************/
var jsWatchTarget = rootFolder + 'js/**/*.js'
var jsTaskTarget = rootFolder + 'js/app/**/*.js'

var jsFrameworksDist = './' + rootFolder + 'js/frameworks/'

var jsDestDist = './/' + rootFolder + 'dist/'
var jsDestProd = '../prod/' + rootFolder + 'dist/'
var jsDestBackend = backendFolder + rootFolder + 'dist'

var jsMakeProd = false
var jsMakeBackend = true

var jsFrameworks = [
    jsFrameworksDist + 'jquery/jquery-2.1.4.min.js',
    jsFrameworksDist + 'materialeze/bin/materialize.min.js',
    jsFrameworksDist + 'underscore/underscore-min.js',
    jsFrameworksDist + 'backbone/backbone-min.js',
]
var jsConcatDev = "main.js"
var jsConcatProd = jsDestProd + "main.js"
var jsConcatbackend = jsDestBackend + "main.js"

jsFrameworks.push(jsTaskTarget);

/************ JS TeMPLATE ***************/
var jsTemplateWatchTarget = rootFolder + 'js/**/*.html';
var jsTemplateDestDist = './' + rootFolder + 'dist/';

var jsTemplateDestBackend = backendFolder + rootFolder + "dist/";

/************ HTML ***************/
var htmlWatchtarget = rootFolder + '/*.html'
var htmlMakeProd = false
var htmlMakeBackend = false

var htmlDestProd = ''
var htmlDestBackend = ''

// sass
gulp.task('sass', function () {
    var ggg = gulp.src(sassTarget)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(sassDestDev));
    if (sassMakeProd) {
        ggg.pipe(gulp.dest(sassDestProd))
    }
    if (sassMakeBackend) {
        ggg.pipe(gulp.dest(sassDestBackend));
    }
});

gulp.task('otherFiles', function () {
    //gulp.src(['static/sf/sass/normalize.css'])
    //    .pipe(gulp.dest('./static/sf/css/'))
    //    .pipe(gulp.dest('./../prod/static/sf/css'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp/static/sf/css'));
    //gulp.src(['static/sf/sass/frameworks/bootstrap/bootstrap.css', 'static/sf/sass/frameworks/bootstrap/font-awesome.min.css'])
    //    .pipe(gulp.dest('./static/sf/css/frameworks/bootstrap'))
    //    .pipe(gulp.dest('./../prod/static/sf/css/frameworks/bootstrap'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp/static/sf/css/frameworks/bootstrap'));
    //gulp.src(['static/sf/sass/frameworks/loader/loaders.css'])
    //    .pipe(gulp.dest('./static/sf/css/frameworks/loader'))
    //    .pipe(gulp.dest('./../prod/static/sf/css/frameworks/loader'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp/static/sf/css/frameworks/loader'));

    if (fontMakeBackend) {
        gulp.src(fontWatchTarget)
            .pipe(gulp.dest(fontDestBackend))
    }

    //gulp.src('**/*.eot')
    //    .pipe(gulp.dest('./../prod'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp'));
    //gulp.src('**/*.svg')
    //    .pipe(gulp.dest('./../prod'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp'));
    //gulp.src('**/*.ttf')
    //    .pipe(gulp.dest('./../prod'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp'));
    //gulp.src('**/*.woff')
    //    .pipe(gulp.dest('./../prod'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp'));
    //gulp.src('**/*.woff2')
    //    .pipe(gulp.dest('./../prod'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp'));
    //gulp.src('static/sf/sass/frameworks/fonts/**')
    //    .pipe(gulp.dest('./../prod/static/sf/css/frameworks/fonts/'))
    //    .pipe(gulp.dest('static/sf/css/frameworks/fonts/'))
    //    .pipe(gulp.dest('./../../sapfreelance/src/main/webapp/static/sf/css/frameworks/fonts/'));
});

//html
gulp.task('html', function () {
    //gulp.src('**/*.html')
    //    .pipe(gulp.dest('./../prod'))
    //    .pipe(rename(function (path) {
    //        path.extname = ".jsp"
    //    }))
    //.pipe(gulp.dest('./../../sapfreelance/src/main/webapp/WEB-INF/com/ub/sapfreelance/client'));


});

/****************** IMG ************************/
gulp.task('img', function () {
    var ggg = gulp.src(imgTaskTarket)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
    if (imgMakeProd) {
        ggg.pipe(gulp.dest(imgDestProd))
    }
    if (imgMakeBackend) {
        ggg.pipe(gulp.dest(imgDestBackend));
    }
});

/****************** JS ************************/
gulp.task('js', function () {
    var ggg = gulp.src(jsFrameworks)
        .pipe(sourcemaps.init())
        .pipe(uglify().on('error', gulpUtil.log))
        .pipe(concat(jsConcatDev))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(jsDestDist))

    if (jsMakeProd) {
        ggg.pipe(concat(jsConcatProd))
        ggg.pipe(gulp.dest(jsDestProd))
    }
    if (jsMakeBackend) {
        ggg.pipe(concat(jsConcatbackend))
        ggg.pipe(gulp.dest(jsDestBackend));
    }


});

/******************* JS TEMPLATE *****************/
gulp.task('jsTemplate', function () {
    var ggg = gulp.src(jsTemplateWatchTarget)
        .pipe(gulp.dest(jsTemplateDestDist))

    if (jsMakeBackend) {
        ggg.pipe(gulp.dest(jsTemplateDestBackend));
    }
});

//Watch task
gulp.task('default', ['sass', 'html', 'img', 'js', 'otherFiles', "jsTemplate"], function () {
    gulp.watch(sassWathTarget, ['sass']);
    gulp.watch(htmlWatchtarget, ['html']);
    gulp.watch(imgWatchTarget, ['img']);
    gulp.watch(jsWatchTarget, ['js']);
    gulp.watch(fontWatchTarget, ['otherFiles']);
    gulp.watch(jsTemplateWatchTarget, ['jsTemplate']);
});

// server
gulp.task('webserver', function () {
    gulp.src('./')
        .pipe(webserver({
            livereload: false,
            directoryListing: false,
            open: '/',
            root: './',
            fallback: 'index.html'
        }));
});
gulp.task('webserverprod', function () {
    gulp.src('./')
        .pipe(webserver({
            livereload: false,
            directoryListing: false,
            open: true,
            root: './../prod',
        }));
});
